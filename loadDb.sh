#!/bin/sh -e

# Ensure the main database has the proper schema
echo "Creating the database schema"
docker exec -it mysql sh -c "/scripts/restore_dump_main.sh"

echo "Done"
exit
