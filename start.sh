#!/bin/bash

##INIT.SH <BRANCH NAME> <SSH KEY>
echo "RUNNING init.sh!!!! DID YOU MAKE SURE DOCKER IS RUNNING?!?!?!"
sleep 6

if [ ! -d ../horecacrowdfunding ]; then
   git clone https://novatio.git.beanstalkapp.com/horecacrowdfunding.git ../horecacrowdfunding/
else
    cd ../horecacrowdfunding
    git fetch --all
fi
if [ ! -d ../visa-service-desk ]; then
    git clone https://novatio.git.beanstalkapp.com/visa-service-desk.git ../visa-service-desk/
else
    cd ../apiRepo
    git fetch --all
fi
if [ ! -d ../mygranolo ]; then
    git clone https://novatio.git.beanstalkapp.com/mygranolo.gitt ../mygranolo/
else
    cd ../mygranolo
    git fetch --all
fi
