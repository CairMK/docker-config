echo "ALERT ALERT ALERT"
echo "this will delete your containers, images, AND all docker data"
echo "docker will probably need to be restarted"
echo "ABORT COMMAND NOW IF YOU DO NOT WANT THIS TO RUN"
sleep 10
./stopDockerContainers.sh && \
  ./removeExitedContainers.sh && \
  ./removeAllImages.sh && \
  rm -rf $HOME/Library/Containers/com.docker.docker/Data/* && \
  echo "all docker data removed... you probably need to restart docker"
