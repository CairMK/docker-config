# dockerConfigs

dockerConfigs is responsible for creating the service stack for LumenAd local development

## Prerequisites
* For Intel machines, make sure VT-x (hardware virtualization) is enabled in the BIOS for your machine
* For AMD machines, make sure SVM is enabled in the BIOS for your machine

* Node 8+ - https://nodejs.org/en/download/
* Git must be installed
    * Ubuntu: `apt-get install git`
    * Mac: Open a terminal and run `git` - it should automatically prompt you to install the xcode command line tools. Follow the prompts
    * Windows: https://git-scm.com/download/win
* Install Docker
	* Windows 10 Pro: https://docs.docker.com/docker-for-windows/install/
    * Mac: https://docs.docker.com/docker-for-mac/install/
    * Linux: Some distros offer their own apt/yum repos for installing
        * Ubuntu: https://docs.docker.com/install/linux/docker-ce/ubuntu/
    * Windows 10 Home machines are not supported. It should work using Docker Toolbox (which uses VirtualBox instead of Hyper-V) but requires subtle changes and scripting might not work.
    * Be sure to sign in to Docker Desktop when prompted, otherwise hub.docker.com won't let you pull some images
* Extra steps on Windows
    * Edit your %PATH% environment variable - add `C:\Program Files\Git\bin`
* Configure the LumenAd VPN
    * https://lumenad.atlassian.net/wiki/spaces/DO/pages/167575566/VPN+Setup
	    * It is preferred to use the official OpenVPN client instead of Tunnelblick for best compatibility 
* Make sure your local repositories are on master branches and up-to-date: `git checkout master` on each of the repos.
* Install parcel globally 
    * `npm install -g parcel` (you might need to use sudo)
* Install cross-env globally
    * `npm install -g cross-env`

## Process
* Clone this repo 
    * `git clone git@bitbucket.org:LumenAd_Platform/dockerconfigs.git dockerConfigs`
* Connect to the LumenAd VPN
* Open a bash or shell terminal
    * On Mac or Linux, any bash terminal will work
    * On Windows, you'll need to start a command prompt first, then run `sh`
        * If running `sh` says "command not found", you didn't edit your %PATH% correctly. See above
        * You can't just use the Git Bash prompt on the start menu - it doesn't provide a real TTY
* `cd dockerConfigs`
* If you don't already have ../apiRepo/ and ../reactRepo/ folders, run `./init.sh` to pull these locally. This is where your dev code lives and will be mapped into the container
* Run `./rebuildLocal.sh` - this will build all the containers
    * You can disconnect from the VPN now, if desired
* Wait ~5 minutes while the application builds
* Run `docker logs -f react` to watch node build the front-end application. You cannot access it until this finishes
* Once the logs stop, go to http://localhost:8080

## Optional
* The data is from April 2019. To move the data foward to be current access your Api container: from the dockerConfigs root `./accessApiContainer.sh` then run the artisan command `php artisan demo:shift 30` where 30 is equal to the amount of days you would like to move the data forward
* Optional, to enable hot reloading from the dockerConfigs folder: `docker-compose stop react && cd ../reactRepo/ && npm ci && npm run dev`

## Troubleshooting

* Double check the following
    * Docker is running
    * You're logged into to Docker Desktop with your hub.docker.com creds
    * You are connected to the VPN - https://lumenad.atlassian.net/wiki/spaces/DO/pages/167575566/VPN+Setup
    * You can resolve build.lumenad.com - DNS problems over VPN are  common
    * On Windows, you have added to your %PATH% ";C:\Program Files\Git\bin"
        * Windows doesn't have a bash shell, so we use Git Bash 
* On Windows, if you receive errors about "winpty" or "the input device is not a TTY"
    * This is caused by running Git Bash directly. Most scripts require a real TTY
        * Run `cmd.exe` first, then `sh`, then `./rebuildLocal.sh`
    * You have permission to execute the scripts
        * `chmod +x *.sh` will add execute permission
* Reboot. No joke, we're actually serious.
* Run `docker logs -f react` to see if the project is still building