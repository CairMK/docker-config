#!/bin/bash
echo "Default sql DB_PASSWORD=user1 / DB_USERNAME=user1 / DB_HOST=127.0.0.1 and DB are visa, mygranolo, horecacrowdfunding "
sleep 6

function wait_for_container {
    until [ "`docker inspect -f {{.State.Running}} ${1}`"=="true" ]; do
        echo " - Waiting for '${1}' container to finish starting..."
        sleep 0.5;
    done;
    echo " - '${1}' container running..."
}

docker-compose down --rmi local -v
./init2.sh

# switch to the local mapped folders
wait_for_container mysql
wait_for_container visa
wait_for_container mygranolo
wait_for_container horecacrowdfunding
wait_for_container overload
wait_for_container papilon
docker exec -it visa sh -c "composer install"
docker exec -it visa sh -c "chmod 755 /var/www && chown -R www-data:www-data /var/www"
## docker exec -it visa sh -c "php artisan migrate --seed --force"
docker exec -it mygranolo sh -c "composer install"
docker exec -it mygranolo sh -c "chmod 755 /var/www && chown -R www-data:www-data /var/www"
## docker exec -it mygranulo sh -c "php artisan migrate --seed --force"
docker exec -it horecacrowdfunding sh -c "composer install"
docker exec -it horecacrowdfunding sh -c "chmod 755 /var/www && chown -R www-data:www-data /var/www"
## docker exec -it horecacrowdfunding sh -c "php artisan migrate --seed --force"
docker exec -it overload sh -c "composer install"
docker exec -it overload sh -c "chmod 755 /var/www && chown -R www-data:www-data /var/www"
docker exec -it papilon sh -c "composer install"
docker exec -it papilon sh -c "chmod 755 /var/www && chown -R www-data:www-data /var/www"
docker exec -it papilon sh -c "php artisan migrate --seed"

